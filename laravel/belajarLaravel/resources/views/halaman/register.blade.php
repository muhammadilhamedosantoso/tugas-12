<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman Form</title>
</head>
<body>
    <h1>Buat Akun Baru</h1>
    <h3>Sign Up Form</h3>
    <form action="/kirim" method="post">
    @csrf
    <label for="fullname">First Name</label><br>
    <input type="text" name="fname" id="firstname"><br><br>
    <label for="lastname">Last Name</label><br>
    <input type="text" name="lname" id="lastname"><br><br>
    <label>Gender</label><br>
    <input type="radio" name="jk" value="1">Man<br>
    <input type="radio" name="jk" value="2">Woman<br>
    <input type="radio" name="jk" value="3">Other<br><br>
    <label>Nationality</label>
    <select name="ngr">
    <option value="indonesia">Indonesia</option><br><br>
    <option value="singapura">Singapura</option><br><br>
    <option value="malaysia">Malaysia</option><br><br>
    <option value="thailand">Thailand</option><br><br>
    </select><br><br>
    <label>Language Spoken</label> <br>
    <input type="checkbox" name="bhs" value="indonesia">Bahasa Indonesia <br>
    <input type="checkbox" name="bhs" value="english">English <br>
    <input type="checkbox" name="bhs" value="arab">Arab <br>
    <input type="checkbox" name="bhs" value="japanese">Japanese<br><br>
    <label>Alamat</label><br>
    <textarea name="address" cols="30" rows="5"></textarea><br><br>
    <button type="submit">Kirim</button>
    </form>
</body>
</html>